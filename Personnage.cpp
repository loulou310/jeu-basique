#include "Personnage.hpp"
#include <string>
#include <iostream>

Personnage::Personnage() : m_nom("Personnage"), m_vie(100), m_mana(100), m_arme("Epee rouillée", 10) {}

Personnage::Personnage(std::string nomPersonnage, std::string nomArme, int degatArme): m_nom(nomPersonnage), m_vie(100), m_mana(100), m_arme(nomArme, degatArme) {}

Personnage::~Personnage(){

}

void Personnage::recevoirDegat(int nbDegat)
{
    m_vie -= nbDegat;

    if (m_vie < 0) {
        m_vie = 0;
    }
}


void Personnage::attaquer(Personnage &cible)
{
    std::cout << getNom() << " attaque " << cible.getNom() << std::endl;
    std::cout << cible.getNom() << " subit " << m_arme.getDegat() << " points de degats\n";
    cible.recevoirDegat(m_arme.getDegat());
}

void Personnage::attaqueMagique(Personnage &cible)
{
    std::cout << getNom() << " utiliste un attaque magique sur " << cible.getNom() << std::endl;
    std::cout << cible.getNom() << " subit 30 points de degats" << std::endl;
    cible.recevoirDegat(30);
    m_mana -= 50;

    if(m_mana < 0) {
        m_mana = 0;
    }
}

void Personnage::boirePotionDeVie(int vieRestauree)
{
    std::cout << getNom() << " boit une potion de vie et recupere " << vieRestauree << " points de vie\n";

    m_vie += vieRestauree;

    if (m_vie > 100)
    {
        m_vie = 100;
    }
    
}

void Personnage::boirePotionDeMana(int manaRestaure)
{

    std::cout << getNom() << " boit une potion de mana et recupere " << manaRestaure << " points de mana\n";

    m_mana += manaRestaure;

    if(m_mana > 100)
    {
        m_mana = 100;
    }
}

void Personnage::changerArme(std::string nomNouvelleArme, int degatNouvelleArme)
{
    std::cout << getNom() << " change d'arme et manie maintenant " << nomNouvelleArme << " infligant " << degatNouvelleArme << " points de dégat\n";
    m_arme.changer(nomNouvelleArme, degatNouvelleArme);
}

bool Personnage::estVivant() const
{
    return m_vie > 0;
}

void Personnage::afficherEtat()
{
    if (estVivant())
    {
    std::cout << getNom() << " :" << std::endl;
    std::cout << "Statut : Vivant\n";
    std::cout << "Vie : " << m_vie << std::endl;
    std::cout << "Mana : " << m_mana << std::endl;
    m_arme.afficher();  
    }
    else
    {
        std::cout << getNom() << " :" << std::endl;
        std::cout << "Status : Mort" << std::endl;
    }
    
}

std::string Personnage::getNom() {
    return m_nom;
}
