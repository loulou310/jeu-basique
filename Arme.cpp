#include "Arme.hpp"
#include <string>
#include <iostream>

Arme::Arme() : m_nom("Epee rouilée"), m_degat(10) {}

Arme::Arme(std::string nom, int degat): m_nom(nom), m_degat(degat) {}

void Arme::changer(std::string nom, int degat) 
{
    m_nom = nom;
    m_degat = degat;
}

void Arme::afficher() const
{
    std::cout << "Arme : " << m_nom << std::endl << "Degats : " << m_degat << std::endl; 
}

int Arme::getDegat() const
{
    return m_degat;
}