#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE

#include <string>
#include "Arme.hpp"

class Personnage
{
    public:

    Personnage();
    Personnage(std::string nomPersonnage, std::string nomArme, int degatArme);
    ~Personnage();
    void recevoirDegat(int nbDegat);
    void attaquer(Personnage &cible);
    void attaqueMagique(Personnage &cible);
    void boirePotionDeVie(int vieRestauree);
    void boirePotionDeMana(int manaRestauree);
    void changerArme(std::string nomNouvelleArme, int degatNouvelleArme);
    void afficherEtat();
    bool estVivant() const;
    std::string getNom();

    private:

    std::string m_nom;
    int m_vie;
    int m_mana;
    Arme m_arme;
};

#endif