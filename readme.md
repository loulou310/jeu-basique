# Jeu basique

Jeu très basique réalisé en c++

Téléchargement : [https://framagit.org/loulou310/jeu-basique/-/releases](https://framagit.org/loulou310/jeu-basique/-/releases)

## Build

Prérequis : 

- g++
- mingw-w64
- make

Build : 

Executez `make`. Les fichiers de sortie se trouvent dans `build`.